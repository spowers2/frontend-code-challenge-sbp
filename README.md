# Accelerator Frontend Developer Interview Challenge

The task is to implement a **2 step UI wizard** to create a user account. There is no UX or UI constraints, this is 
up to you to decide. 

The User information that we need to collect is described in the User type:
```
interface User {
  name: string
  age: number
  email: string
  newsletter: 'daily' | 'weekly' | 'monthly'
}
```
You can, for example collect the name and age in the first step and then email and newsletter in the second step.
You may use a routing library such that every step is a separate route but this is completely optional and not 
required.

There is a dummy `project` package(implemented in the /project folder) which exports a `createUser` function. This function returns a `Promise`.
Use it to simulate a request that creates a user account. 
Ex:

```
import { createUser } from 'project'

const details = {...}

createUser(details).then( ... )
```

The focus should be on code style and the way you approach the problem implementation wise.
Feel free to use any other helper library although ideally the more code you write yourself the better.

### Implementation requirements:

- use either vanilla Javascript or one of the frameworks we use on the Accelerator project(React / TypeScript)
- use npm to manage dependencies, there is pre-initialized package.json included in this repo

### Feel free to:
- use [create-react-app](https://github.com/facebookincubator/create-react-app) or webpack as a build tool
- use a statically typed language like Typescript or simply [flowtype](https://flowtype.org/)

## Additional Instructions

- Fork this repository

- After you're done, provide us the link to your repository.

- Implementations without a README will be automatically rejected.

- No pull requests

- Asking questions is good. We will not penalize you for asking questions. In an actual work environment, we prefer that engineers spend 30 minutes talking a problem over with someone than spend an entire day building the wrong thing.

- We like code that is clean and easily testable. We honestly don't always do a great job of this ourselves, so your ability to demonstrate this is a big plus.

- Please comment your code.

- If you find this exercise too difficult or if you get stuck somewhere, feel free to talk to us. While we obviously prefer candidates who can hit the ground running, we're also willing to consider candidates who can, with some help, learn on the job.

## Bonus Points

- Clean code!
- Knowledge of application flow.
- Knowledge of modern best practices/coding patterns.
- Componential thinking.

Optional: build the project and deploy (ie make it available as a static project) on 
[Github Pages](https://pages.github.com/), otherwise please provide detailed instructions
on how to start the project locally.
